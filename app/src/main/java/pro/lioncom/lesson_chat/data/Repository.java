package pro.lioncom.lesson_chat.data;

import android.util.Log;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import pro.lioncom.lesson_chat.App;
import pro.lioncom.lesson_chat.data.newsModel.Response;
import pro.lioncom.lesson_chat.data.room.Entry;

public class Repository implements RepositoryContract {
    @Override
    public Single<List<Entry>> getMock() {
        return Mock.getInstance().getList();
    }

    @Override
    public void insertListEntry(List<Entry> list) {
        App.getDao().insertListEntry(list);
    }

    @Override
    public void insertEntry(Entry entry) {
        App.getDao().insertEntry(entry);
    }

    @Override
    public void deleteEntry(Entry entry) {App.getDao().deleteEntry(entry); }

    @Override
    public void updateEntry(Entry entry) {
        Log.d("DEBUG","editUser "+entry);
        App.getDao().updateEntry(entry); }

    @Override
    public Flowable<List<Entry>> getEntryListFromDatabase() {
        return App.getDao().getEntryList();
    }

    @Override
    public Single<Response> getNews(String value, Boolean allNews) {
        if(allNews){
            Log.d("DEBUG","repo get all News: ");
            return App.getRestService().getService().getAllNews();
        }else {
            return App.getRestService().getService().getNews(value);
        }
    }

//    @Override
//    public Single<Entry> queryEntry(String key) {
//        return App.getDao().queryEntry(key);
//    }
//
//    @Override
//    public Single<List<Entry>> getEntryListFromMock() {
//        return Mock.getInstance().getList();
//    }
}
