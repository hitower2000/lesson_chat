package pro.lioncom.lesson_chat.data.room;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Objects;

@Entity(tableName = "entry")
public class Entry {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private String lName;
    private String fName;
    private int age;
    private String url;

    @Ignore
    public Entry() {
    }

    public Entry(@NonNull String lName, String fName, int age, String url) {
        this.lName = lName;
        this.fName = fName;
        this.age = age;
        this.url = url;
    }

    @Override
    public String toString() {
        return lName +" "+ fName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry entry = (Entry) o;
        return lName.equals(entry.lName) &&
                Objects.equals(fName, entry.fName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lName, fName);
    }
    public void setAge(int age) {
        this.age = age;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getFName() {
        return fName;
    }

    public String getLName() {
        return lName;
    }

    public String getUrl() {
        return url;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}