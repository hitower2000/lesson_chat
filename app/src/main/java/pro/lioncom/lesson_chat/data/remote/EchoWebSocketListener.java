package pro.lioncom.lesson_chat.data.remote;

import android.util.Log;

import com.google.gson.JsonObject;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

   public class EchoWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            JsonObject json = new JsonObject();
            json.addProperty("login","user1");
            webSocket.send(json.toString());

            JsonObject json1 = new JsonObject();
            json1.addProperty("msg","message from user1");
            webSocket.send(json1.toString());
        }
        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Log.d("DEBUG","Receiving : " + text);
//            JsonObject json = new JsonObject();
//            json.addProperty("msg","message from user1");
//            webSocket.send(json.toString());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            Log.d("DEBUG",("Closing : " + code + " / " + reason));
        }
        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            Log.d("DEBUG",("Error : " + t.getMessage()));
        }
    }

