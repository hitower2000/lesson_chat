package pro.lioncom.lesson_chat.data.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface LocalServiceEntry {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListEntry(List<Entry> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEntry(Entry entry);

    @Delete
    void deleteEntry(Entry entry);

    @Update (onConflict = OnConflictStrategy.REPLACE)
    void updateEntry(Entry entry);

    @Query("select * from entry")
    Flowable<List<Entry>> getEntryList();

    @Query("SELECT * FROM entry WHERE lName IS :lName")
    Single<Entry> queryEntry(String lName);
}
