package pro.lioncom.lesson_chat.data;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import pro.lioncom.lesson_chat.App;
import pro.lioncom.lesson_chat.data.room.Entry;

public class Mock {
    private List<Entry> list;
    private static Mock instance;

    private Mock() {
        list = new ArrayList<>();
    }

    public static synchronized Mock getInstance() {
        if (instance == null) {
            instance = new Mock();
        }
        return instance;
    }

    public Single<List<Entry>> getList() {
        list.add(new Entry("pupkin", "vasya", 18, "https://f1gr.hjfile.cn/pic/20170906/201709060335318628.gif"));
        list.add(new Entry("poreshenko", "petya", 50, "https://www.ccn.com/wp-content/uploads/2019/05/charlie-lee-1.jpg"));
        list.add(new Entry("korzik", "ivar", 25, "https://s1.1zoom.ru/prev2/536/Tigers_Painting_Art_Night_Glance_535374_300x200.jpg"));
        list.add(new Entry("durov", "boris", 32, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjP-lkXQ9E8dw6O1k4WdONZ1eOtrvQWtLB9PSqucyCArgSbQLC5g"));

        return Single.just(list);
    }

}

