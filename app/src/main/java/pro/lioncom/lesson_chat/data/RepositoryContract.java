package pro.lioncom.lesson_chat.data;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import pro.lioncom.lesson_chat.data.newsModel.Response;
import pro.lioncom.lesson_chat.data.room.Entry;

public interface RepositoryContract {
    Single<List<Entry>> getMock();

    void insertListEntry(List<Entry> list);

    void insertEntry(Entry entry);
    void deleteEntry(Entry entry);
    void updateEntry(Entry entry);

    Flowable<List<Entry>> getEntryListFromDatabase();

    Single<Response> getNews(String value, Boolean allNews);
}
