package pro.lioncom.lesson_chat.data.remote;

import io.reactivex.Single;
import pro.lioncom.lesson_chat.data.newsModel.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    //@GET("everything?q={value}&apiKey=e5b738f8eeb44e0cb17994ad8caf6081")
    @GET("everything")
    Single<Response> getNews(@Query("q") String value);

    @GET("top-headlines?country=ua")
    Single<Response> getAllNews();

//    @POST("/api/cmd/{as}")
//    Single<RequestExample> updateSettings(@Path("as") String val, @Header("X-Terminal-SN") String sn, @Body RequestExample requestExample);
}