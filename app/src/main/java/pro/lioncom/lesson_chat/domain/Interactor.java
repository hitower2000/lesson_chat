package pro.lioncom.lesson_chat.domain;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import pro.lioncom.lesson_chat.data.Repository;
import pro.lioncom.lesson_chat.data.RepositoryContract;
import pro.lioncom.lesson_chat.data.newsModel.Response;
import pro.lioncom.lesson_chat.data.room.Entry;

public class Interactor extends BaseInteractor implements InteractorContract {
    private RepositoryContract repository;

    public Interactor() {
        repository = new Repository();
        //Log.d("DEBUG","new Interactor ");
    }

    @Override
    public void insertMockToDatabase() {
        repository.getMock()
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<List<Entry>>() {
                    @Override
                    public void onSuccess(List<Entry> list) {
                        repository.insertListEntry(list);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public Flowable<List<Entry>> getList() {
        return repository.getEntryListFromDatabase()
                .compose(applyFlowableSchedulers());
    }


    @Override
    public void addUser(Entry user) {
         Completable.fromAction(() -> repository.insertEntry(user)).subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void deleteEntry(Entry user) {
        Completable.fromAction(() -> repository.deleteEntry(user)).subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void updateEntry(Entry user) {
        Completable.fromAction(() -> repository.updateEntry(user)).subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public Single<Response> getNews(String value, Boolean allNews) {
        return Single.timer(5, TimeUnit.MILLISECONDS)
        .flatMap(t -> repository.getNews(value,allNews))
                .compose(applySingleSchedulers());
    }
}

//    @Override
//    public Single<List<Entry>> insertListEntry(List<Entry> list) {
//        Completable.fromAction(() -> repository.insertListEntry(list))
//                .subscribeOn(Schedulers.io())
//                .subscribe(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        Log.e("insertListEntry", "" + list.toArray().toString());
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("insertListEntry", "" + e.getMessage());
//                    }
//                });
//        return queryEntryList();
//    }

//    @Override
//    public Single<List<Entry>> queryEntryList() {
//        return repository.getEntryListFromDatabase()
//                .compose(applySingleSchedulers());
//    }