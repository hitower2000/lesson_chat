package pro.lioncom.lesson_chat.domain;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import pro.lioncom.lesson_chat.data.newsModel.Response;
import pro.lioncom.lesson_chat.data.room.Entry;

public interface InteractorContract {
    Flowable<List<Entry>> getList();

 //   Single<List<Entry>> insertListEntry(List<Entry> list);
//
//    Single<List<Entry>> queryEntryList();

    void insertMockToDatabase();

    void addUser(Entry user);
    void deleteEntry(Entry user);
    void updateEntry(Entry user);


    Single<Response> getNews(String value, Boolean allNews);
}
