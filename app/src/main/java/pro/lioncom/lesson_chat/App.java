package pro.lioncom.lesson_chat;

import android.app.Application;
import android.arch.persistence.room.Room;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import pro.lioncom.lesson_chat.data.remote.EchoWebSocketListener;
import pro.lioncom.lesson_chat.data.remote.RestService;
import pro.lioncom.lesson_chat.data.room.LocalServiceEntry;
import pro.lioncom.lesson_chat.data.room.PersonDataBase;

public class App extends Application {
    public static App app;
    private static LocalServiceEntry dao;
    private static RestService restService;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        dao = provideLocaleDao(provideRoomDataBase(this));
        restService = new RestService.Builder(BuildConfig.SERVER_URL).build();

        //test websocket
        OkHttpClient client = new OkHttpClient();
        //Request request = new Request.Builder().url("ws://echo.websocket.org").build();
        Request request = new Request.Builder().url("ws://ts.bigtrade.sale:58080/examples/websocket/chat").build();
        EchoWebSocketListener listener = new EchoWebSocketListener();
        WebSocket ws = client.newWebSocket(request, listener);
        client.dispatcher().executorService().shutdown();
    }

    public static RestService getRestService() {
        return restService;
    }

    private PersonDataBase provideRoomDataBase(App context) {
        return Room.databaseBuilder(
                context.getApplicationContext(), PersonDataBase.class, Const.NAME_DAO)
                .allowMainThreadQueries()
                .build();
    }

    private LocalServiceEntry provideLocaleDao(PersonDataBase dao) {
        return dao.getPaxDao();
    }

    public static LocalServiceEntry getDao() {
        return dao;
    }
}
