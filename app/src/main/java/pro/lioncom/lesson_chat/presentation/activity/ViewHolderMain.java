package pro.lioncom.lesson_chat.presentation.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import pro.lioncom.lesson_chat.R;
import pro.lioncom.lesson_chat.data.room.Entry;
import pro.lioncom.lesson_chat.databinding.ItemBinding;

public class ViewHolderMain extends RecyclerView.ViewHolder {
    private final ItemBinding binding;

    public ViewHolderMain(@NonNull View itemView) {
        super(itemView);
        this.binding = DataBindingUtil.bind(itemView);
    }

    public void bind(Entry item) {
        //Log.d("DEBUG","bind url: "+item.getUrl());
        //binding.setEntry(item);
        //binding.executePendingBindings();
        if (item != null) {
            binding.lName.setText(item.getLName());
            binding.fName.setText(item.getFName());
            binding.age.setText(String.valueOf(item.getAge()));

            setImageWhithGlide(item.getUrl(),binding.image);

            binding.image.setOnClickListener(v -> {
                AlertDialog.Builder builder= new AlertDialog.Builder(itemView.getContext());
                View fullScreenView = LayoutInflater.from(itemView.getContext()).inflate(R.layout.full_screen_image,null);
                builder.setView(fullScreenView);
                ImageView image = fullScreenView.findViewById(R.id.imageView);

                setImageWhithGlide(item.getUrl(),image);
                builder.create().show();
            });
       }
    }

    private void setImageWhithGlide(String url, ImageView view){
        Glide.with(itemView)
                .load(url)
                .centerCrop()
                .into(view);
    }
}
