package pro.lioncom.lesson_chat.presentation.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pro.lioncom.lesson_chat.R;
import pro.lioncom.lesson_chat.data.newsModel.ArticlesItem;
import pro.lioncom.lesson_chat.data.room.Entry;
import pro.lioncom.lesson_chat.databinding.ActivityMainBinding;
import pro.lioncom.lesson_chat.databinding.EditItemDialogBinding;
import pro.lioncom.lesson_chat.presentation.Utils;
import pro.lioncom.lesson_chat.presentation.base.BaseActivity;
import pro.lioncom.lesson_chat.presentation.base.BasePresenter;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements PresenterContract.View {
    private PresenterContract.Presenter presenter;
    private Adapter adapter;
    private static final int MY_CAMERA_PERMISSION_CODE = 1;
    private static final int CAMERA_REQUEST = 2;
    private EditItemDialogBinding itemView;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);

        presenter = new Presenter();
        presenter.init();
        getBinding().setEvent(presenter);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, getBinding().drawerLayout, getBinding().toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        getBinding().drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        getBinding().rvMain.setLayoutManager(layoutManager);
        adapter = new Adapter(new ArrayList<>(),presenter);
        getBinding().rvMain.setAdapter(adapter);
    }

    @Override
    public void updateListItems(List<Entry> list) {
//        adapter.setList(list);
 //       adapter.notifyDataSetChanged();
    }



    @Override
    public void UpdateNews(List<ArticlesItem> articlesItemsList) {
        adapter.setList(articlesItemsList);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.starView(this);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onStartView() {

    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onPause() {
        presenter.stopView();
        super.onPause();
    }

    @Override
    public void openDrawer() {
        getBinding().drawerLayout.openDrawer(Gravity.START);
    }

    @Override
    public void closeDrawer() {
        getBinding().drawerLayout.closeDrawers();
    }

    @Override
    public void ShowAddUserDialog(Entry entry) {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        View customDialogView = LayoutInflater.from(this).inflate(R.layout.edit_item_dialog, null);
        itemView = DataBindingUtil.bind(customDialogView);
        itemView.setItemDialog(presenter);
        itemView.agePicker.setMinValue(18);
        itemView.agePicker.setMaxValue(60);
        itemView.agePicker.setWrapSelectorWheel(false);
        builder.setView(customDialogView);
        builder.setTitle(R.string.add_user_title);
        if (entry != null){
            builder.setTitle(R.string.edit_user_title);
            itemView.lName.setText(entry.getLName());
            itemView.fName.setText(entry.getFName());
            itemView.agePicker.setValue(entry.getAge());
            if(entry.getUrl()!=null) {
                Bitmap myBitmap = BitmapFactory.decodeFile(entry.getUrl());
                if(myBitmap!=null){
                    itemView.image.setImageBitmap(myBitmap);
                }else{
                    itemView.image.setImageDrawable(getDrawable(R.drawable.ic_launcher_foreground));
                }
            }else{
                itemView.image.setImageDrawable(getDrawable(R.drawable.ic_launcher_foreground));
                Log.d("DEBUG","entry.getUrl()=null ");
            }
        }
        builder.setPositiveButton("OK", (dialog, which) -> {
            if (entry != null){
                entry.setlName(textViewToString(itemView.lName));
                entry.setfName(textViewToString(itemView.fName));
                entry.setAge(itemView.agePicker.getValue());
                if(imagePath!=null) {
                    entry.setUrl(imagePath);
                }
                presenter.editUser(entry);
            }else{
                Entry user = new Entry(textViewToString(itemView.lName)
                        ,textViewToString(itemView.fName)
                        , itemView.agePicker.getValue(), imagePath);
                presenter.addUser(user);
            }
            imagePath = null;
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            if(imagePath!=null){
                File imageFile = new File(imagePath);
                if(imageFile.exists()){
                    imageFile.delete();
                }
                imagePath = null;
            }
        });
        builder.create().show();
    }

    @Override
    public void pickPhoto() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        }
        else
        {
            startPhotoIntent();
        }
    }

    @Override
    public void openUrl(String URL) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(URL));
        startActivity(i);
    }

    private String textViewToString(EditText v){
        return  v.getText().toString();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                //Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                startPhotoIntent();
            }
            else
            {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
   }

   private void startPhotoIntent(){
       Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
       startActivityForResult(cameraIntent, CAMERA_REQUEST);
   }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
         if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            itemView.image.setImageBitmap(photo);
            imagePath = Utils.saveToFile(photo);
            //Log.d("DEBUG","imagePath: "+imagePath);
        }
    }
}
