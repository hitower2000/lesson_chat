package pro.lioncom.lesson_chat.presentation.base;

public interface BasePresenter<T> {
    void starView(T view);
    void stopView();
}
