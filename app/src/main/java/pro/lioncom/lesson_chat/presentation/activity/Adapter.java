package pro.lioncom.lesson_chat.presentation.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pro.lioncom.lesson_chat.R;
import pro.lioncom.lesson_chat.data.newsModel.ArticlesItem;

public class Adapter extends RecyclerView.Adapter<ViewHolderNews> {
    private List<ArticlesItem> list;
    private PresenterContract.Presenter presenter;

    public Adapter(List<ArticlesItem> list, PresenterContract.Presenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }

    public void setList(List<ArticlesItem> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderNews onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false);
        return new ViewHolderNews(view,presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderNews viewHolderNews, int i) {
        viewHolderNews.bind(list.get(i));
//        viewHolderNews.itemView.setOnLongClickListener(v -> {
//            AlertDialog.Builder builder = new AlertDialog.Builder(viewHolderNews.itemView.getContext());
//
//            builder.setTitle(R.string.action_title);
//            builder.setItems(R.array.action_menu, (dialog, itemPosition) -> {
//                switch (itemPosition){
//                    case 0:
//                        listener.editUserButtonClick(list.get(i));
//                        break;
//                    case 1:
//                        showDeleteUserDialog(i, viewHolderNews.itemView.getContext());
//                        break;
//                }
//            });
//            builder.show();
//            return true;
//        });


    }

//    private void showDeleteUserDialog(int position, Context context) {
//        AlertDialog.Builder deleteAlert = new AlertDialog.Builder(context);
//        //deleteAlert.setTitle(R.string.deleteAlert_title);
//        deleteAlert.setMessage(context.getString(R.string.deleteAlert_message, list.get(position).toString()));
//        deleteAlert.setPositiveButton("OK",(dialog, which) -> listener.deleteEntry(list.get(position)));
//        deleteAlert.setNegativeButton("Cancel",null);
//        deleteAlert.show();
//     }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
