package pro.lioncom.lesson_chat.presentation;

import android.graphics.Bitmap;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;

public class Utils {
    public static String saveToFile(Bitmap photo){
        String filename = System.currentTimeMillis()+".jpg";
        File sd = Environment.getExternalStorageDirectory();
        File dest = new File(sd, filename);
        try {
            FileOutputStream out = new FileOutputStream(dest);
            photo.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return dest.getPath();
    }
}
