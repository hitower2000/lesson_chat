package pro.lioncom.lesson_chat.presentation.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import pro.lioncom.lesson_chat.data.newsModel.ArticlesItem;
import pro.lioncom.lesson_chat.databinding.ItemNewsBinding;

public class ViewHolderNews extends RecyclerView.ViewHolder {
    private final ItemNewsBinding binding;
    private PresenterContract.Presenter presenter;

    public ViewHolderNews(@NonNull View itemView, PresenterContract.Presenter presenter) {
        super(itemView);
        this.binding = DataBindingUtil.bind(itemView);
        this.presenter = presenter;
    }

    public void bind(ArticlesItem item) {
        //Log.d("DEBUG","bind url: "+item.getUrl());
        //binding.setEntry(item);
        //binding.executePendingBindings();
        if (item != null) {
            binding.author.setText(item.getAuthor());
            binding.title.setText(item.getTitle());
            binding.description.setText(item.getDescription());

            if(item.getUrlToImage()!=null) {
                setImageWhithGlide(item.getUrlToImage(), binding.image);
             }else{
                setImageWhithGlide("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png", binding.image);
            }

            binding.frameLayout.setOnClickListener(v -> {
                presenter.openUrl(item.getUrl());
            });
 //           binding.image.setOnClickListener(v -> {
//                AlertDialog.Builder builder= new AlertDialog.Builder(itemView.getContext());
//                View fullScreenView = LayoutInflater.from(itemView.getContext()).inflate(R.layout.full_screen_image,null);
//                builder.setView(fullScreenView);
//                ImageView image = fullScreenView.findViewById(R.id.imageView);

//                setImageWhithGlide(item.getUrlToImage(),image);
//                builder.create().show();
 //           });
       }
    }

    private void setImageWhithGlide(String url, ImageView view){
        Glide.with(itemView)
                .load(url)
                .centerCrop()
                .into(view);
    }
}
