package pro.lioncom.lesson_chat.presentation.activity;

import android.util.Log;

import io.reactivex.disposables.CompositeDisposable;
import pro.lioncom.lesson_chat.data.room.Entry;
import pro.lioncom.lesson_chat.domain.Interactor;
import pro.lioncom.lesson_chat.domain.InteractorContract;

public class Presenter implements PresenterContract.Presenter{
    private PresenterContract.View view;
    private CompositeDisposable disposable;
    private InteractorContract interactor;

    public Presenter() {
        disposable = new CompositeDisposable();
        interactor = new Interactor();
    }

    @Override
    public void init() {
        //interactor.insertMockToDatabase();
//        disposable.add(interactor.getList()
//                .subscribe(list -> view.updateListItems(list),throwable -> Log.d("DEBUG","error presenter")));

        disposable.add(interactor.getNews("", true)
                .subscribe(response->view.UpdateNews(response.getArticles()), throwable -> Log.d("DEBUG","error get all news")));
    }

    @Override
    public void starView(PresenterContract.View view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
    }

    @Override
    public void openDrawer() {
        view.openDrawer();
    }

    @Override
    public void closeDrawer() {
        view.closeDrawer();
    }

    @Override
    public void addUserButtonClick(){
        view.ShowAddUserDialog(null);
        view.closeDrawer();
    }

    @Override
    public void editUserButtonClick(Entry user) {
        view.ShowAddUserDialog(user);
    }

    @Override
    public void addUser(Entry user) {
        interactor.addUser(user);
    }

    @Override
    public void startSearch(String searchText) {
        disposable.add(interactor.getNews(searchText, false).subscribe(response->view.UpdateNews(response.getArticles()), throwable -> Log.d("DEBUG","error get news")));
        Log.d("DEBUG","startSearch: "+searchText);
    }

    @Override
    public void pickPhoto() {
        Log.d("DEBUG","PickPhoto");
        view.pickPhoto();
    }



    @Override
    public void deleteEntry(Entry entry) {
        interactor.deleteEntry(entry);
    }

    @Override
    public void openUrl(String URL) {
        view.openUrl(URL);
    }


    @Override
    public void editUser(Entry entry) {
        interactor.updateEntry(entry);
    }
}
