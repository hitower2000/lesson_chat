package pro.lioncom.lesson_chat.presentation.activity;

import java.util.List;

import pro.lioncom.lesson_chat.data.newsModel.ArticlesItem;
import pro.lioncom.lesson_chat.data.room.Entry;
import pro.lioncom.lesson_chat.presentation.base.BasePresenter;


public interface PresenterContract {
    interface View{
        void updateListItems(List<Entry> list);
        void openDrawer();
        void closeDrawer();
        void ShowAddUserDialog(Entry entry);
        void pickPhoto();
        void openUrl(String URL);
        void UpdateNews(List<ArticlesItem> listItems);
    }

    interface  Presenter extends BasePresenter<View> {
        void init();
        void openDrawer();
        void closeDrawer();
        void addUserButtonClick();
        void editUserButtonClick(Entry entry);
        void editUser(Entry entry);
        void addUser(Entry user);
        void startSearch(String searchText);
        void pickPhoto();
        void deleteEntry(Entry entry);
        void openUrl(String URL);
    }
}
